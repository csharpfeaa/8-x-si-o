﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XsiO
{
    public partial class Form1 : Form
    {
        String jucatorCurent = "x";
        string[,] joc = new string[3, 3];
        bool jocTerminat = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void MarkTheSpot(object sender, EventArgs e)
        {
            if (jocTerminat)
                return;

            string[] pozitionare = ((Button)sender).Tag.ToString().Split(',');

            // daca deja avem o valoare pentru casuta curenta, ignora
            if (joc[int.Parse(pozitionare[0]), int.Parse(pozitionare[1])] != null)
                return;

            // marcam pozitia pe care a fost dat click cu semnul jucatorului curent
            joc[int.Parse(pozitionare[0]), int.Parse(pozitionare[1])] = jucatorCurent;

            ((Button)sender).Text = jucatorCurent.ToUpper();

            // verifica daca am castigat
            bool castigator = verificaSolutie(jucatorCurent);

            if (castigator)
            {
                jocTerminat = true;
                MessageBox.Show(jucatorCurent.ToUpper() + " a castigat.");
            }

            // schimba jucatorul
            jucatorCurent = jucatorCurent == "x" ? "o" : "x";
        }

        private bool verificaSolutie(string jucator)
        {
            int countDiag = 0;
            int countDiagInv = 0;

            for (int i = 0; i < 3; i++)
            {
                int countLinie = 0;
                for (int j = 0; j < 3; j++)
                {
                    if (jucator == joc[i, j])
                        countLinie++;

                    if (i == j && jucator == joc[i, j])
                        countDiag++;

                    if (j == joc.GetLength(1) - 1 - i && jucator == joc[i, j])
                        countDiagInv++;
                }

                if (countLinie == 3)
                    return true;
            }

            if (countDiag == 3 || countDiagInv == 3)
                return true;

            for (int i = 0; i < 3; i++)
            {
                int countCol = 0;
                for (int j = 0; j < 3; j++)
                {
                    if (jucator == joc[j, i])
                        countCol++;
                }

                if (countCol == 3)
                    return true;
            }

            return false;
        }

        private void jocNouToolStripMenuItem_Click(object sender, EventArgs e)
        {
            jocTerminat = false;
            joc = new string[3, 3];

            // parcurgem toate controalele din formular
            // in cazul butoanelor, resetam proprietatea Text
            foreach (Object b in this.Controls)
            {
                if (b is Button)
                {
                    // facem cast obiectului b -> Button
                    ((Button) b).Text = "";
                }

            }
        }

        private void despreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("May the Force be with you.", "Despre");
        }
    }
}
